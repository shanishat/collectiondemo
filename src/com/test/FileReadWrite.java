package com.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReadWrite {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File f = new File("E:\\test\\test.txt");
		System.out.println("No of lines written = "+write(f));
		System.out.println("No of lines read out = "+read(f));
	}
	
	public static int write(File file){

		int count=0;
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(file,true));

			for(int i=0 ; i<3;i++){
				bw.write("Shahid");
				bw.newLine();
				count++;
			}
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			System.out.println("File is written successfully");
		}
		return count;
		
	}
	
	public static int read(File file){

		int count = 0;
		String line = "";
		try {
			BufferedReader bw = new BufferedReader(new FileReader(file));			  
			while(null != (line  = bw.readLine()))
			{
				System.out.println(line);
				count++;
			}
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			System.out.println("File read is done successfully");
		}
		return count;
	}
		

}
