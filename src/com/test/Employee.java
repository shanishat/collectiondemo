/**
 * 
 */
package com.test;

/**
 * @author MD
 *
 */
public class Employee implements Comparable<Employee>{
	private String name;
	private int id;
	
	
	public Employee(String name, int id) {
		super();
		this.name = name;
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Employee [name=" + name + ", id=" + id + "]";
	}
	@Override
	public int compareTo(Employee o) {
		// TODO Auto-generated method stub
		if(this.getId() > o.getId())
			return 1;
		if(this.getId() < o.getId())
			return -1;
		else
			return 0;
	}
	
	

}
