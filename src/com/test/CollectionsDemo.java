package com.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


public class CollectionsDemo {

	public static void main(String[] args) {
		//// TODO Auto-generated method stub
		String name = "shahid";
		String name1 = new String("shahid");
		System.out.println(name +"&"+name1);
		if(name==name1){
			System.out.println("both the string is true");
		}
		if(name.equals(name1))
		{
			System.out.println("true");
		}
		System.out.println("Default size of Collection framework \n"+"Java 6: \n"+
				"Vector = 10 \n"+
				"Arraylist = 10 \n"+
				"Hashtable = 11 \n"+
				"Hashmap = 16 \n"+
				"Hashset = 16 \n"+
				"Java 7: \n"+
					"Vector = 10 \n"+
					"Arraylist = 0. Size will change to 10 once we add the element in the list \n"+
					"Hashtable = 11 \n"+
					"Hashmap = 0. Size will change to 16 once we add the element in the map \n"+
					"Hashset = 0. Size will change to 16 once we add the element in the set");
		ListExample();
		//SetExample();
		//MapExample();
		//Use of generic
		add(2.0,3.3);
		
		//Multiple inheritance
		TruckImpl imp  = new TruckImpl();
		imp.sub(6, 5);
	}
	private static void MapExample() {
		// TODO Auto-generated method stub
		Map<String, String> hasMap = new HashMap<>();
		hasMap.put("FNAME", "SHAHID");
		hasMap.put("LNAME", "HOSSAIN");
		hasMap.put("ADDRESS", "KOLKATA");
		hasMap.put("SALARY", "100000");
		System.out.println("1st way of Itertion for Map");
		for(Entry<String, String> element:hasMap.entrySet()){
			System.out.println("Key = "+element.getKey() + " And Value = "+element.getValue() );
		}
		for(String key:hasMap.keySet()){
			System.out.println("Key = "+key);
		}
		for(String value:hasMap.values()){
			System.out.println("Value = "+value);
		}
	}
	private static void SetExample() {
		// TODO Auto-generated method stub
		Set<String> hashSet = new HashSet<>();
		hashSet.add("Shahid");
		hashSet.add("Hossain");
		hashSet.add("KOLKATA");
		hashSet.add("100000");
		
		Iterator<String> itr = hashSet.iterator();
		System.out.println("1st way of Set iteration");
		while(itr.hasNext()){
			System.out.println(itr.next());
		}
		System.out.println("2nd way of Set iteration");
		for(String element:hashSet){
			System.out.println(element);
		}
		System.out.println("3rd way of Set iteration");
		hashSet.forEach(element->System.out.println(element));
		
	}
	public static void ListExample(){
		List<String> list = new ArrayList<>();
		List<Employee> empList  = new ArrayList<>();
		list.add("Shahid");
		list.add("Hossain");
		list.add("KOLKATA");
		list.add("100000");
		
		empList.add(new Employee("shahid",110));
		empList.add(new Employee("kashif",199));
		empList.add(new Employee("aqueel",99));
		empList.add(new Employee("irshad",23));
		
		//Iteration
		Iterator<String> itr = list.iterator();
		System.out.println("1st way of list iteration");
		while(itr.hasNext()){
			System.out.println(itr.next());
		}
		System.out.println("2nd way of list iteration");
		for(String element:list){
			System.out.println(element);
		}
		System.out.println("3rd way of list iteration");
		for(int i = 0 ; i<list.size() ; i++){
			System.out.println(list.get(i));
		}
		System.out.println("4th way of list iteration");
		list.forEach(element->{System.out.println(element);});
		
		//Sorted elements are
		
		Collections.sort(list);
		
		System.out.println("Elements before sorting");
		empList.forEach(element->{System.out.println(element);});
		list.forEach(element->{System.out.println(element);});
		//empList.sort(new IdComparator());
		empList.sort(new NameComparator());
		//Collections.sort(empList);
		Collections.sort(list);
		System.out.println("Elements after sorting");
		empList.forEach(element->{System.out.println(element);});
		list.forEach(element->{System.out.println(element);});
		
	}
	public static <T,K> void add(T a, K b){
		
		System.out.println("Sum is = "+a + b);
	}

}
