/**
 * 
 */
package com.test;

/**
 * @author MD
 *
 */
public class RemoveDuplicateElements {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] arr  = new int[]{1,1,2,3,4,5,3,8};
		removeDuplicate(arr);
		reverseString("Shahid");
	}
		public static void removeDuplicate(int[] arr){
	    int end = arr.length;
	    System.out.println("Array before removing duplicate elements");
	    for(int i = 0; i < end ;i++){
	    	System.out.println(arr[i]);
	    }
	    for (int i = 0; i < end; i++) {
	        for (int j = i + 1; j < end; j++) {
	            if (arr[i] == arr[j]) {                  
	                int shiftLeft = j;
	                for (int k = j+1; k < end; k++, shiftLeft++) {
	                    arr[shiftLeft] = arr[k];
	                }
	                end--;
	                j--;
	            }
	        }
	    }
	    System.out.println("Array after removing duplicate elements");
	    for(int i = 0; i < end ;i++){
	    	System.out.println(arr[i]);
	    }
		}
		public static void reverseString(String string){
			int len = string.length();
			String reverseString = "";
			for(int i = len-1 ; i>= 0; i--){
				reverseString = reverseString+string.charAt(i);
			}
			System.out.println("Reverse String is = "+reverseString);
		}

	}

