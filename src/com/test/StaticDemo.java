package com.test;

public class StaticDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Static members share common memory, static memebers does not belong to any perticular object rather all objects");
		System.out.println("Static method can only access static variable and static methods");
		System.out.println("Non-Static method can access static variable and static methods");
		System.out.println("Only way to access non static Member from static methods is to create object first like below");
		StaticDemo std = new StaticDemo();
		sub(2,3);
		std.add(2,3);
	}
	public void add(int a, int b) {
		System.out.println("Sum of two number is = "+(a+b));
		//for(int i=0;i<1;i++)
		//main(new String[2]);
		
	}
	public static void sub(int a, int b){
		System.out.println("Sub is = "+(b-a));
	}

}
