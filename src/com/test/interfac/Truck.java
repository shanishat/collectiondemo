/**
 * 
 */
package com.test.interfac;

/**
 * @author MD
 *
 */
public interface Truck {
	default void sub(int a , int b){
		System.out.println("in Truck");
	}
	void add(int a, int b);
}
