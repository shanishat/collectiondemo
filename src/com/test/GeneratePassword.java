package com.test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.security.SecureRandom;

public class GeneratePassword {



		private static final String FILENAME = "E:\\test\\filename.txt";
	    private static final String ALPHA_CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    private static final String ALPHA = "abcdefghijklmnopqrstuvwxyz";
	    private static final String NUMERIC = "0123456789";
	    private static final String SPECIAL_CHARS = "!@#$%^&*_=+-/";

		public static void main(String[] args) {

			BufferedWriter bw = null;
			FileWriter fw = null;

			try {

				String content = "This is the content to write into file";

				fw = new FileWriter(FILENAME,true);
				bw = new BufferedWriter(fw);
				int len=8;
				for(int i=0;i<=500000;i++){
					
					 String password = generatePassword(len, ALPHA_CAPS + ALPHA+ NUMERIC);
					bw.write(password);
					bw.newLine();
				}
				System.out.println("Done");
				bw.close();
				fw.close();

			} catch (IOException e) {

				e.printStackTrace();

			} /*finally {

				try {

					if (bw != null)
						bw.close();

					if (fw != null)
						fw.close();

				} catch (IOException ex) {

					ex.printStackTrace();

				}

			}*/

		}
		public static String generatePassword(int len, String dic) {
		    String result = "";
		    SecureRandom random = new SecureRandom();
		    for (int i = 0; i < len; i++) {
		        int index = random.nextInt(dic.length());
		        result += dic.charAt(index);
		    }
		    return result;
		    }



}
